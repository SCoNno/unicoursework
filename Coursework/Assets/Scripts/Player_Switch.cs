﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Switch : MonoBehaviour
{

    bool playerActive1;
    bool playerActive2;

    GameObject findPlayer1;
    GameObject findPlayer2;

    public GameObject player1Sprite;
    public GameObject player2Sprite;

    Rigidbody2D rb;

    //sets player 1 active and player 2 false
    public void Start()
    {
        playerActive1 = true;
        playerActive2 = false;
        findPlayer1 = GameObject.Find("Player1");
        findPlayer2 = GameObject.Find("Player2");
        findPlayer2.GetComponent<Player_Movement>().enabled = false;
        player2Sprite.GetComponent<SpriteRenderer>().enabled = false;
    }

    //switch's players
    private void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            playerActive1 = true;
            playerActive2 = false;
            if(playerActive1 == true)
            {
                switchPlayer();
            }
        }
        if (Input.GetKeyDown("2"))
        {
            playerActive2 = true;
            playerActive1 = false;
            if(playerActive2 == true)
            {
                switchPlayer();
            }
        }
    }
    

    //enables and disables the player movement scripts on either player.
    //Also sprite renderer which shows what player you are controlling
    //sets mass to 1000 so the player cant move the other while not in control
    void switchPlayer()
    {
        if(playerActive1 == true)
        {
            findPlayer1.GetComponent<Player_Movement>().enabled = true;
            findPlayer2.GetComponent<Player_Movement>().enabled = false;
            player1Sprite.GetComponent<SpriteRenderer>().enabled = true;
            player2Sprite.GetComponent<SpriteRenderer>().enabled = false;
            findPlayer2.GetComponent<Rigidbody2D>().mass = 1000;
            findPlayer1.GetComponent<Rigidbody2D>().mass = 1;
        }
        if(playerActive2 == true)
        {
            findPlayer1.GetComponent<Player_Movement>().enabled = false;
            findPlayer2.GetComponent<Player_Movement>().enabled = true;
            player1Sprite.GetComponent<SpriteRenderer>().enabled = false;
            player2Sprite.GetComponent<SpriteRenderer>().enabled = true;
            findPlayer1.GetComponent<Rigidbody2D>().mass = 1000;
            findPlayer2.GetComponent<Rigidbody2D>().mass = 1;
        }
    }
}
