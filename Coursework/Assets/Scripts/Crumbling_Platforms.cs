﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crumbling_Platforms : MonoBehaviour
{
    public int maxHealth = 500;
    public int currentHealth;
    public bool isPlatformTouched = false;
    public int playersHit = 0;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Player is on platform. Adds 1 to variable
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlatformTouched = true;
            playersHit += 1;
           
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //player leaves the platforms
        playersHit -= 1;
    }

    private void FixedUpdate()
    {
        //checks if one player is on the platform. Starts to decrement
        if(playersHit == 1)
        {
            PlatformDamage();
           
        }
        //Checks if two players are on the platform. Health decrementing stops and the alpha fading stops
        if(playersHit == 2)
        {
            //isPlatformTouched = false;
            currentHealth +=0;
            StopAllCoroutines();
        }
        //check if the platform was touchd by player and then left the platform to continue to damage it again.
        if(isPlatformTouched == true && playersHit <= 1)
        {
            PlatformDamage();
        }
        //kills platform
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
    
    //Damage for the platform. Alpha fading.
    void PlatformDamage()
    {
        currentHealth--;
        StartCoroutine(FadeTo(0.0f,5.0f));
    }
    
    //gets the sprite colour Alpha, loops through the health over time to lerp from starting value to end value to show that the platform is degrading
    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = gameObject.GetComponent<SpriteRenderer>().color.a;
        for (float t = 0.0f; t < maxHealth; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            gameObject.GetComponent<SpriteRenderer>().color = newColor;
            yield return null;
        }
    }
}
