﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Show_UI : MonoBehaviour
{
    //takes the text object
    public GameObject uiObject;

    private void Start()
    {
        //sets it to false
        uiObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //sets true when player triggers it
        if (collision.gameObject.CompareTag("Player"))
        {
            uiObject.SetActive(true);
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        //sets false when player leaves
        if (collision.gameObject.CompareTag("Player"))
        {
            uiObject.SetActive(false);
        }
    }
}
