﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bouncing : MonoBehaviour
{
    public float bounceAmount;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //adds force to players y
        if (collision.gameObject.CompareTag("Bounce"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bounceAmount);
        }

    }
}
