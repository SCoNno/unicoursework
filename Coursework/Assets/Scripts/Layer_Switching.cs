﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layer_Switching : MonoBehaviour
{
    bool purpleActive;
    bool redActive;

    GameObject findPurplePlatform;
    GameObject findRedPlatform;

    public GameObject[] platforms;


    public void Start()
    {
        purpleActive = true;
        redActive = false;
        //put second lot of redplats in this section
        platforms[0].gameObject.SetActive(false);
        //put second lot of purpplats in here
        platforms[3].gameObject.SetActive(false);
    }

    //Q button used to active the switch, setting boolens
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && purpleActive == false && redActive == true)
        {
            purpleActive = true;
            redActive = false;
            colourSwitch();
        }
        else if (Input.GetKeyDown(KeyCode.Q) && purpleActive == true && redActive == false)
        {
            purpleActive = false;
            redActive = true;
            colourSwitch();
        }
    }

    //preforms the colour switch
    void colourSwitch()
    {
        if (purpleActive == true)
        {
            platforms[1].gameObject.SetActive(true);
            platforms[0].gameObject.SetActive(false);
            platforms[2].gameObject.SetActive(true);
            platforms[3].gameObject.SetActive(false);
        }
        if (redActive == true)
        {
            platforms[1].gameObject.SetActive(false);
            platforms[0].gameObject.SetActive(true);
            platforms[3].gameObject.SetActive(true);
            platforms[2].gameObject.SetActive(false);
        }
    }
}
