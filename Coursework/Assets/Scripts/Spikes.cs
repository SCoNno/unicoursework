﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spikes : MonoBehaviour
{
    public int playerHealth = 1;

    //Collsion with player to deal damage
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Spikes"))
        {
            playerHealth -= 1;
        }
        if(playerHealth == 0)
        {
            RestartLevel();
        }
    }
    //Restarts level
    public void RestartLevel()
    {
        // load the nextlevel
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
