﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Next_Level : MonoBehaviour
{
    //variable to check how many times the portal comes in to contact with players
    public int touchedPortal = 0;


    //When player comes into contact adds 1 to variable. Allows to check once both players have touched it to move to next level
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            touchedPortal += 1;
            Debug.Log(touchedPortal);
        }
        if (touchedPortal == 2)
        {
            LoadLevel();
        }
    }

    //loads next level in the build index
    public void LoadLevel()
    {
        // load the nextlevel
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }
}
