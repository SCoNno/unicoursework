﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce_Pad : MonoBehaviour
{
    
    public float bounceAmount;
    public GameObject bouncePad;
    public GameObject[] triggers;

    private void Start()
    {
        bouncePad.gameObject.SetActive(false);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //Activate bounce pad, removes the second trigger
        if (collision.gameObject.name == "Trigger")
        {
            bouncePad.gameObject.SetActive(true);
            triggers[1].gameObject.SetActive(false);
            Debug.Log("Open");
        }
        //activates bounce pad, removes first trigger
        if (collision.gameObject.name == "Trigger2")
        {
            bouncePad.gameObject.SetActive(true);
            triggers[0].gameObject.SetActive(false);
            Debug.Log("Open2");
        }
        //force on player y
        if (collision.gameObject.CompareTag("Bounce"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bounceAmount);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
            //deactivates bouncepad and sets triggers to true
            if (collision.gameObject.name == "Trigger")
            {
                bouncePad.gameObject.SetActive(false);
                triggers[1].gameObject.SetActive(true);
            }
            if (collision.gameObject.name == "Trigger2")
            {
                bouncePad.gameObject.SetActive(false);
                triggers[0].gameObject.SetActive(true);
            }
    }


   
}
