﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    public float moveSpeed;
    public bool isGrounded;

    public Rigidbody2D rb;

    private void Update()
    {
        Movement();
        Jumping();
    }

    void Movement()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += movement * Time.deltaTime * moveSpeed;

        //Movement in one direction
        /*if (Input.GetKey(KeyCode.D))
        {
            Vector3 velocity = new Vector2(moveSpeed, 0);
            transform.position += velocity * Time.deltaTime * moveSpeed;
        }*/
    }

    void Jumping()
    {
        //Player has to be on floor to have jump ability
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 6f), ForceMode2D.Impulse);
        }
    }


    //Ground checking functions 
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
            Debug.Log("ground");
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
            Debug.Log("not ground");
        }
    }
}
